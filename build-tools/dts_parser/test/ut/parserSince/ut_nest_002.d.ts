/**
 * the ut for parent node has since tag version, and some child nodes's version is less than parent version
 * 
 * @since 7
 */
export class Test {
  /**
   * @since 6
   */
  id: number;
  /**
   * @since 7
   */
  name: string;
  /**
   * @since 8
   */
  age: number;
}