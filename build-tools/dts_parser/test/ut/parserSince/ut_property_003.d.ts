/**
 * the ut for property in interface, the property is readonly
 */
export interface test {
  readonly name: string
}