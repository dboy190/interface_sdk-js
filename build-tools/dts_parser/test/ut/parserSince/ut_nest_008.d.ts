/**
 * the ut for parent node has two or more JsDoc, some child nodes have two or more JsDoc
 * 
 * @since 7
 */
/**
 * @since 8
 */
export enum Test {
  /**
   * @since 6
   */
  /**
   * @since 7
   */
  id,
  
  /**
   * @since 7
   */
  /**
   * @since 8
   */
  name,

  /**
   * @since 7
   */
  age = 4,

  /**
   * @since 9
   */
  address,

  /**
   * @since 9
   */
  school = 5,

  /**
   * @since 9
   */
  city,
}