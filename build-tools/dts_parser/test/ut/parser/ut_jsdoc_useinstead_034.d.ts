/**
 * the ut for jsdoc about useinstead
 *
 * @since 7
 * @deprecated since 9
 * @useinstead ohos.app.ability.dataUriUtils/Test
 */
export namespace test {}
