/**
 * the ut for jsdoc about default in interface
 *
 */
export interface Test {
  /**
   * @default 0
   */
  defaultValue: number;
}
