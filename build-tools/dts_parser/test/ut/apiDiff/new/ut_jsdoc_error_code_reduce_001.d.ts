/**
 * the ut for jsdoc about reduce throws
 *
 */
export class Test {
  /**
   * @throws { BusinessError } 201 - Permission denied.
   * @throws { BusinessError } 202 - non-system app called system api.
   */
  func(str: string): void;
}
