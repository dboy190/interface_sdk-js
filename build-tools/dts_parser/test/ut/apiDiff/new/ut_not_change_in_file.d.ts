export namespace test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS
   */
  function func(str: string): void;

  interface test1 {
    func1(str: string):void;
  }
}